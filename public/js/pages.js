wmControllers.controller('PagesCtrl', ['$scope', '$filter', '$rootScope', '$location', 'wmApi', function($scope, $filter, $rootScope, $location, wmApi) {

  // $scope.adminEditMode = true;
  $scope.api = wmApi("pages",$scope);
  $scope.list = [];
  $scope.api.getShortlist(function(){
  $scope.member = {usr: "",pass:""};
    $scope.menuFromMainList();

    //open page on load

    $scope.pageActive = $location.path().substr(1);
    var path = $scope.pageActive

    if($scope.pageActive==""){
      $scope.pageActive = "home";
    }
    if(path != ""){
      var page = $scope.api.getItemByUrl(path);
      if(page!=undefined){
        $scope.goToPage(page)
      }
    }else{
      $scope.goToPage( $scope.listTop[0] )
    }

  });


  $scope.menuFromMainList = function(){
    $scope.listTop = $filter('filter')($scope.list, {linkType:0});
    $scope.listLeft = $filter('filter')($scope.list, {linkType:1});
    $scope.listHidden = $filter('filter')($scope.list, {linkType:2});
  }
  $scope.getTitle = function(item){
    return item.name;
  }
  $scope.addPage = function(){
    $scope.api.addItem({name: $scope.newPageName});
    $scope.newPageName = "";
  }

  $scope.goToPage = function(page){
    $rootScope.$broadcast('pageWasChanged', page);
    $("title").text($rootScope.siteName+" "+page.name);
  }

  $scope.checkMember = function(){

    if($scope.member.usr == "student" && $scope.member.pass == "gbgbestschool"){
      $scope.pageActive = "96746c70b82560058f33d6e42d69c0db";
      $scope.goToPage($scope.api.getItemByUrl($scope.pageActive))
    }else{
      $scope.member.usr = "";
      $scope.member.pass = "";
    }
  }


  $scope.sortableOptions = {
      connectWith: ".page-container",
      items:".sortablePage",
      start: function(event, ui){
      },
      beforeStop: function(event, ui){

        var movedItem = $scope.api.getItemById(ui.helper[0].id.replace("Link",""));
        if (movedItem) {
          var newColumn = parseInt(ui.helper.parent()[0].getAttribute('menu-type'));
          if (movedItem.linkType !== newColumn) {
           movedItem.linkType = newColumn;
           $scope.api.saveItem({id:movedItem.id, linkType:movedItem.linkType},false)
          }
        }
      },
      stop: function(event, ui){
        var i = 0;
        $scope.list = [];
        $scope.listTop.forEach(function(el){
          if(el.priority != i){
            el.priority = i;
            $scope.api.saveItem({id:el.id, priority:el.priority},false)
          }
          $scope.list.push(el);
          i++;
        })
        $scope.listLeft.forEach(function(el){
          if(el.priority != i){
            el.priority = i;
            $scope.api.saveItem({id:el.id, priority:el.priority},false)
          }
          $scope.list.push(el);
          i++;
        })
      },
    };
  $scope.$on('itemWasUpdated', function(event, item) {
    $scope.api.refreshItemInList(item);
    $scope.menuFromMainList();
  });
  $scope.$on('itemWasAdded', function(event, item) {
    // $scope.list.push(item);
    $scope.goToPage(item)
    $scope.menuFromMainList();
  });
  $scope.$on('itemWasRemoved', function(event, pageId) {
    $scope.api.removeItemInList(pageId);
    $scope.menuFromMainList();
  });
  $scope.$on('adminStatusWasChanged', function(event, admin) {
  });
  $scope.$on('$locationChangeStart', function(next, current) {
    if($scope.list.length == 0){
      return;
    }
    $scope.pageActive = $location.path().substr(1);
    if($scope.pageActive === ""){
      $scope.pageActive = $scope.listLeft[0].url;
    }
    var page = $scope.api.getItemByUrl($scope.pageActive);
    if(page != undefined){
      $scope.goToPage( page )
    }
  });


}]);

// ---------------------------------------------------------------------------------------------------


wmControllers.controller('PageCtrl', ['$scope', '$sce', 'wmApi', function($scope, $sce, wmApi){

  $scope.api = wmApi("pages",$scope);
  $scope.menuTypes = [
        {name:"Top Menu", val:0},
        {name:"Left Menu", val:1},
        {name:"Hidden", val:2},
      ];
  $scope.newPage = function(){
   $scope.data = {};
  };
  $scope.newPage();

  $scope.saveLavel = function(){
   return !$scope.onSync?'Сохранить':'Подождите...';
  }

  $scope.getContent = function(){
    // console.log($scope.data.content);
   return $sce.trustAsHtml($scope.data.content);
  }

  $scope.savePage = function(){
   // console.log($scope.data);
   $scope.api.saveItem($scope.data);
  }


  $scope.summernoteOptions = {
      toolbar: [
        ["style", ["bold", "italic", "underline",  "strikethrough", "clear", "style"]],
        ["color", ["color"]],
        ["para", ["ul", "ol", "paragraph"]],
        ["insert", [ "link", "picture", "table", "hr"]],
        ["misc", ["codeview"]]
      ],
      onImageUpload: $scope.api.sendFileFromEditor
  };


  $scope.$on('pageWasChanged', function(event, page) {
    $scope.api.getItem(page.id);
  });
  $scope.$on('itemWasRemoved', function(event, pageId) {
    if($scope.data.id == pageId){
      $scope.newPage();
    }
  });



}]);