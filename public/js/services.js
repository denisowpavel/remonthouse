'use strict';

/* Services */

var wmServices = angular.module('wmServices', []);

wmServices.factory('wmApi', ['$http','$rootScope', function($http, $rootScope){
    return function(modelName, scope){
        
        scope.onSync = false;

        var getIndexById = function(id){
            for (var i = 0; i < scope.list.length; i++) {
                if(scope.list[i].id === id){
                    return i;
                }
            }
            return -1;
        };
        var getIndexByUrl = function(url){
            for (var i = 0; i < scope.list.length; i++) {
                if(scope.list[i].url === url){
                    return i;
                }
            }
            return -1;
        };
        var spliceFromListById = function(id){
            var index = getIndexById(id)
            if(index > 0){
                scope.list.splice(index, 1);    
            }
            
        };

        var pushItemUpdateEvent = function(item){
            $rootScope.$broadcast('itemWasUpdated', item);
        };
        var pushItemAddEvent = function(item){
            $rootScope.$broadcast('itemWasAdded', item);
        };        
        var pushItemRemovedEvent = function(itemId){
            $rootScope.$broadcast('itemWasRemoved', itemId);
        };
        var transliterate = function(word) {
            var A = {};
            var result = '';
            A["Ё"]="YO";A["Й"]="I";A["Ц"]="TS";A["У"]="U";A["К"]="K";A["Е"]="E";A["Н"]="N";A["Г"]="G";A["Ш"]="SH";A["Щ"]="SCH";A["З"]="Z";A["Х"]="H";A["Ъ"]="'";
            A["ё"]="yo";A["й"]="i";A["ц"]="ts";A["у"]="u";A["к"]="k";A["е"]="e";A["н"]="n";A["г"]="g";A["ш"]="sh";A["щ"]="sch";A["з"]="z";A["х"]="h";A["ъ"]="'";
            A["Ф"]="F";A["Ы"]="I";A["В"]="V";A["А"]="A";A["П"]="P";A["Р"]="R";A["О"]="O";A["Л"]="L";A["Д"]="D";A["Ж"]="ZH";A["Э"]="E";
            A["ф"]="f";A["ы"]="i";A["в"]="v";A["а"]="a";A["п"]="p";A["р"]="r";A["о"]="o";A["л"]="l";A["д"]="d";A["ж"]="zh";A["э"]="e";
            A["Я"]="YA";A["Ч"]="CH";A["С"]="S";A["М"]="M";A["И"]="I";A["Т"]="T";A["Ь"]="'";A["Б"]="B";A["Ю"]="YU";
            A["я"]="ya";A["ч"]="ch";A["с"]="s";A["м"]="m";A["и"]="i";A["т"]="t";A["ь"]="'";A["б"]="b";A["ю"]="yu";
            for(var i = 0; i < word.length; i++) {
                var c = word.charAt(i);
                result += A[c] || c;
            }
            return result;
        };        
        var nameToUrl = function(name) {
            name = transliterate(name)
            var url = name.toLowerCase().replace(/[^a-zA-Z0-9]+/g, "-");
            return url
        };       
        var saveExistingItem = function(item,replace){            
            if(replace == undefined){replace = true}
            scope.onSync = true;
            console.log(">",item)
            $http.put('/'+modelName+"/"+item.id,item).success(function(data){                
                scope.onSync = false;
                if(replace){
                    pushItemUpdateEvent(item);
                    console.log("<",data)
                }
            });
        };
        var saveNewItem = function(item){
            scope.onSync = true;
            $http.post('/'+modelName,item).success(function(data){
                if(scope.list != undefined){
                    scope.list.push(data);
                }
                pushItemAddEvent(data);
                scope.onSync = false;
            });            
        };
        var setRedyToRemove = function(id, val){
            if(scope.list != undefined){
                scope.list[getIndexById(id)].redyToRemove = val;
            }
            if(scope.data != undefined){
                scope.data.redyToRemove = val;
            }
        }



        return {
        getShortlist: function(callback) {
            $http.get('/'+modelName+'/?{"$fields":{"content":0},"$sort":{"priority":1}}').success(function(data){
                scope.list = data;
                if(callback!=undefined){callback();}
            });
        },
        getList: function(callback) {
            $http.get('/'+modelName+'/?{"$sort":{"priority":1}}').success(function(data){
                scope.list = data;
                if(callback!=undefined){callback();}
            });
        },
        getItemToList: function(id) {            
            scope.onSync = true;
            $http.get('/'+modelName+"/"+id).success(function(data){
                scope.list[getIndexById(id)] = data;
                scope.onSync = false;
            });
        },
        getItem: function(id) {
            scope.onSync = true;
            $http.get('/'+modelName+"/"+id).success(function(data){
                scope.data = data;
                scope.onSync = false;
            });
        },
        saveItem: function(item,replace) {
            if( item.id!=undefined ){
                saveExistingItem(item,replace)
            }else{
               saveNewItem(item) 
            }
        },
        addItem: function(item) {            
            saveNewItem(item)
        },       
        removeItem: function(id) {            
            $http.delete('/'+modelName+"/"+id).success(function(data){
                if(scope.list != undefined){
                    spliceFromListById(id);
                }                
                pushItemRemovedEvent(id);
            });
        },
        askRemoveItem: function(id) {            
            setRedyToRemove(id,true);
            
        },
        rejectRemoveItem: function(id) {
            setRedyToRemove(id,false);
        },




        sendFileFromEditor: function(files,editor,welEditable) {
          var fd = new FormData()
          for (var i in files) {
              fd.append("uploadedFile", files[i])
          }
          
          var xhr = new XMLHttpRequest();
          xhr.upload.addEventListener("progress", function(data){
              var percent = parseInt(data.loaded / data.total * 100);
              $rootScope.$broadcast("showNoteBar","Uploading "+percent+" %");
          }, false);
          xhr.open('POST', '/pageimages'); 
          xhr.onload = function() {
              $rootScope.$broadcast("hideNoteBar");
              var response = JSON.parse(this.responseText);
              for (var index in response){
                editor.insertImage(welEditable, "upload/"+response[index].filename);
              }
          };
          xhr.onerror = function(err) {
              console.log("Error: ", err);
          }
          xhr.send(fd);
        },
        pushItemUpdateEvent: function(item) {
            pushItemUpdateEvent(item);
        },
        refreshItemInList: function(item) {            
            scope.list[getIndexById(item.id)] = item;
        },        
        getItemById: function(id) {            
            return scope.list[getIndexById(id)];
        },        
        getItemByUrl: function(id) {            
            return scope.list[getIndexByUrl(id)];
        },        
        removeItemInList: function(id) {            
            spliceFromListById(id);
        },
        nameToUrl: function(name) {            
            return nameToUrl(name);
        }

    }
};

}]);
