'use strict';

/* App Module */

var wmApp = angular.module('wmApp', [
  'ui.sortable',
  'summernote',
  'wmControllers',
  'wmServices'
]);



var wmControllers = angular.module('wmControllers', []);

wmControllers.controller('RootCtrl', ['$scope', '$rootScope', function($scope, $rootScope) {

    $rootScope.siteName = "STROY.TOMSK.RU";

    $scope.admin = false;
    $scope.adminData = {};
    $scope.adminData.autoLoginMode = false;
    $scope.adminData.adminName = "admin";
    $scope.adminData.usr = localStorage["adminLogin"];
    $scope.adminData.pass = localStorage["adminPass"];
    $scope.noteBarMessage = "";
    $scope.topPageScroll = true;


    $scope.adminDataToLocalStorage = function(){
        $rootScope.$broadcast('adminStatusWasChanged', $scope.admin);
        localStorage.setItem("adminLogin", $scope.adminData.usr);
        localStorage.setItem("adminPass", $scope.adminData.pass);
        localStorage.setItem("adminMode", $scope.adminData.autoLoginMode);
    }
    $scope.scrollSensetiveMenu = function(){
      $( window ).scroll(function() {
        var scroll = $( window ).scrollTop();
        if(scroll < 100){
          if(!$scope.topPageScroll){
            $scope.topPageScroll = true;
            setTimeout(function(){$scope.$apply()},30);
          }
        }else if($scope.topPageScroll){
          $scope.topPageScroll = false;
          setTimeout(function(){$scope.$apply()},30);
        }
      });
    }

    $scope.adminlogIn = function(){
        dpd.users.login({
          username: $scope.adminData.usr,
          password: $scope.adminData.pass
        }, function(result, error){
            $scope.admin = false;
            if(result != undefined){
              dpd.users.get("me",function(data){
                if(data.username === $scope.adminData.adminName){
                  $scope.admin = true;
                  $scope.adminData.autoLoginMode = true;
                }else{
                  $scope.adminData.usr = "";
                }
                $scope.adminDataToLocalStorage()
                $scope.$apply()
              })
            }
            if(error != undefined && error.status == 401){
              $scope.adminData.usr = "";
              $scope.adminData.pass = "";
              $scope.adminData.autoLoginMode = false;
              $scope.adminDataToLocalStorage()
              $scope.$apply()
            }


        });
    }
    $scope.adminlogOut = function(){
        dpd.users.logout(function(result, error){
            $scope.admin = false;
            $scope.adminData.autoLoginMode = false;
            $scope.adminDataToLocalStorage();
            $scope.$apply()

        });
    }


    if( localStorage["adminMode"] === "true"){
        $scope.adminlogIn();
    }



    $scope.scrollSensetiveMenu();

}]);





